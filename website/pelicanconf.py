SITENAME = 'Maturaarbeit - Freie Software'
SITEURL = 'https://fabio-totti-05.gitlab.io/matura-freie-software'

TIMEZONE = 'Europe/Zurich'
DEFAULT_LANG = 'de'
THEME = 'theme'

PATH = 'content'
STATIC_PATHS = []
INDEX_TITLE = 'Arbeitsjournal'
INDEX_SAVE_AS = 'journal/index.html'
ARTICLE_PATHS = ['journal']
ARTICLE_URL = ARTICLE_SAVE_AS = 'journal/{date:%y%m}-{slug}.html'
ARTICLE_EXTERNAL_BASE = 'https://gitlab.com/fabio-totti-05/matura-freie-software/-/releases'
AUTHOR_URL = AUTHOR_SAVE_AS = ''
CATEGORY_URL = CATEGORY_SAVE_AS = ''
TAG_URL = TAG_SAVE_AS = ''
PAGE_PATHS = ['']
PAGE_URL = PAGE_SAVE_AS = '{slug}.html'

OUTPUT_PATH = '../public'
DIRECT_TEMPLATES = ['index']
DELETE_OUTPUT_DIRECTORY = True

MENUITEMS = [
    ('Home', SITEURL + '/'),
    (INDEX_TITLE, SITEURL + '/journal'),
]

DEFAULT_PAGINATION = False
DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False
RELATIVE_URLS = True

FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
