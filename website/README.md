Project Website
===============

Built with [Pelican](https://docs.getpelican.com).

Development
-----------

```console
python3 -m pip install -r requirements.txt
```

```console
cd website
pelican -r -l
```
