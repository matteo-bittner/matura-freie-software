Title: Maturaarbeit - Freie Software
save_as: index.html

## Fabio Bittner

- [Maturaarbeit](freie-software.pdf) (PDF)
- [Wieso <span class="latex">L<span>a</span>T<span>e</span>X</span>?](arbeiten-mit-latex.html)
- [Arbeitsjournal](journal)
- [Quellcode Repository](https://gitlab.com/fabio-totti-05/matura-freie-software)
