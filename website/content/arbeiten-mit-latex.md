Title: Arbeiten mit <span class="latex">L<span>a</span>T<span>e</span>X</span>

Zum Verfassen von grösseren Arbeiten an der Schule habe ich in den letzten drei
Jahren statt <q>Microsoft Word</q>
<span class="latex">L<span>a</span>T<span>e</span>X</span> in Kombination mit
<q>GitLab</q> verwendet.

[<span class="latex">L<span>a</span>T<span>e</span>X</span>](https://www.latex-project.org/)
ist kein Textverarbeitungsprogramm, sondern eine Art Programmiersprache.
Man schreibt Text in einfachen Textdateien (mit der Endung <q>.tex</q>)
mit einem Texteditor, wie ihn beispielsweise Programmierer verwenden.
Teilen des Textes kann man mit bestimmten Textsequenzen (<q>Befehlen</q>)
eine besondere Bedeutung geben (z.B. Überschrift, Unterüberschrift,
Grafik, Inhaltsverzeichnis, usw.). Mit einem Übersetzungsprogramm
(<q><span class="latex">L<span>a</span>T<span>e</span>X</span> Compiler</q>)
kann man aus dem Text und den enthaltenen Befehlen ein druckfertiges Dokument
(z.B. PDF) generieren.

## Einfaches Arbeiten mit VSCodium

Wir müssen ein Dokument abliefern, am besten ohne Rechtschreibfehler,
und wollen schon während des Schreibens sehen, wie das fertige Dokument
aussehen wird, ohne jedes Mal dafür ein PDF-Dokument generieren zu müssen.
Ausserdem kennen wir die vielen
<span class="latex">L<span>a</span>T<span>e</span>X</span>-Befehle nicht, um
Text zu formatieren, Links und Bilder einzufügen, ein Inhaltsverzeichnis,
Literaturverzeichnis, Abbildungsverzeichnis, usw. darzustellen.
Gibt es Programme, die uns dabei unterstützen können?

[VSCodium](https://vscodium.com) ist ein freier Texteditor; es handelt sich um
die von der Community frei paketierte Version von <q>Microsoft Visual Studio
Code</q>. Mit zwei Plugins für
<span class="latex">L<span>a</span>T<span>e</span>X</span>
(latex-workshop, vscode-ltex) wird er zu einer vollwertigen
Entwicklungsumgebung zum Schreiben von
<span class="latex">L<span>a</span>T<span>e</span>X</span>-Dokumenten,
wenn man sich eine gängige
<span class="latex">L<span>a</span>T<span>e</span>X</span>-Distribution
auf seinem Rechner installiert hat. Eine integrierte Vorschau aktualisiert
das PDF-Dokument während man tippt. Falsch geschriebene Wörter werden blau
unterwellt, auf Mausklick bekommt man passende Korrekturvorschläge.
Ein Seitenpanel bietet
<span class="latex">L<span>a</span>T<span>e</span>X</span>-Befehle,
Symbole zum Einfügen und eine Dokumentstruktur zum Navigieren an.

<figure>
  <img src="{static}/images/latex-codium.png" alt="LaTeX-Plugins im VSCodium Texteditor">
  <figcaption><span class="latex">L<span>a</span>T<span>e</span>X</span>-Plugins im VSCodium Texteditor</figcaption>
</figure>

Bei einem grossen Projekt ist es wichtig, die Übersicht nicht zu
verlieren. Dazu kann man das Dokument in die einzelnen Kapitel
aufteilen, also eine eigene Datei pro Kapitel verwalten, die
[im Hauptdokument inkludiert](https://gitlab.com/fabio-totti-05/matura-freie-software/-/blob/main/freie-software.tex#L122-144) wird.

## Einhalten formaler Vorgaben leicht gemacht

Bei einer wissenschaftlichen Arbeit ist es wichtig, formale Aspekte
einzuhalten. Zum Beispiel müssen das Literaturverzeichnis und alle
Referenzen im Dokument ein bestimmtes Format einhalten.
<span class="latex">L<span>a</span>T<span>e</span>X</span> macht das
Anpassen dieser Ausgabeformate leicht. Entweder gibt es eingebaute
Befehle oder <span class="latex">L<span>a</span>T<span>e</span>X</span>-Pakete
(<q>packages</q>), die eine schnelle Anpassung ermöglichen. Damit genügt
meist das Einfügen weniger Zeilen, um das gewünschte Ergebnis zu erreichen.

<strong>Beispiel:</strong> Angenommen, es ist gefordert, dass die
Abbildungen durchgehend nummeriert werden (statt im Format <q>Abbildung
'Abschnitt'.'Nummer': 'Beschreibung'</q>). Recherchiert man im Internet,
z.B. mit <q>[latex list of figures continuous without section numbering](https://duckduckgo.com/?q=latex+list+of+figures+continuous+without+section+numbering&atb=v292-1&ia=web)</q>,
wird man schnell fündig: Ein Blog, ein Beitrag auf StackExchange und
die Overleaf Dokumentation erklären Lösungsmöglichkeiten, die man
einfach übernehmen kann. Fügt man den entsprechenden Code in das
Hauptdokument ein, passt
<span class="latex">L<span>a</span>T<span>e</span>X</span> sofort alle
Referenzen und das Abbildungsverzeichnis der Arbeit automatisch an.

## Fazit

Ursprünglich kam der Vorschlag zum Verwenden von
<span class="latex">L<span>a</span>T<span>e</span>X</span>
von meinen Eltern. Sie hatten in ihrer Studienzeit schon damit
gearbeitet. Der Einstieg war aber trotzdem schwierig, denn ich musste
ein geeignetes Programm zum Arbeiten mit
<span class="latex">L<span>a</span>T<span>e</span>X</span>
finden. Zum Glück hatte ich das Problem nicht alleine, denn meine
ältere Schwester wollte auch schon mit
<span class="latex">L<span>a</span>T<span>e</span>X</span>
ihre Maturaarbeit schreiben. Die Suche nach den Arbeitsinstrumenten
wurde somit zur Aufgabe für die ganze Familie.

Mit dem fertigen Setup, VSCodium mit Plugins für Rechtschreibprüfung
und automatisches PDF-Generieren, sowie GitLab für die begleitende
Projektwebsite, kam ich mit der Online-Dokumentation und Tutorials im
Internet gut voran. Ich war aber dankbar, wenn ich auf meine
Familienmitglieder zurückgreifen konnte, wenn es Layoutprobleme oder
Fehlermeldungen gab, mit denen ich nicht sofort etwas anfangen konnte.
Eine <q>Community</q> braucht es also zum Überwinden von
Schwierigkeiten und zum Teilen des Erfolgs!

Heute möchte ich das Arbeiten mit
<span class="latex">L<span>a</span>T<span>e</span>X</span>
nicht mehr missen. Das schöne, einheitliche Layout, die Leichtigkeit
und Übersichtlichkeit beim Überarbeiten des Dokuments sind Dinge, die
<q>Microsoft Word</q> nicht leisten kann. Wenn ich uneinheitlich
gelayoutete Dokumente meiner Kolleg:innen sehe und daran denke, wie
sie Rückmeldungen der Lehrperson kurz vor dem Abgabetermin mühsam
einarbeiten müssen, denke ich, dass ich mit
<span class="latex">L<span>a</span>T<span>e</span>X</span>
auf das richtige Pferd gesetzt habe.
