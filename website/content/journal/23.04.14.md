Title: Lösungen für den Schulbetrieb, FUSS-Projekt, Schulen in GB, Website mit SSG Pelican
Date: 2023-04-14
ID: 23.04.14

Das Dokument ist nun in 3 Teile, <q>Theorie</q>, <q>Anwendung</q> und
<q>Analyse</q>, aufgeteilt.

Ein neuer Abschnitt über Adaptive Learning gibt es im Kapitel
<q>Problemstellung</q>, wo auch die Formatierung der Abschnitte geändert wurde
(Sections statt Subsections), um die Lesbarkeit zu verbessern.

Ein Abschnitt über Lösungen für den Schulbetrieb ist im Kapitel <q>Free/Libre
Open Source Projekte</q> neu vorhanden.

Im Kapitel <q>Selbstversuche</q> wurde die Projektwebsite hinzugefügt und der
Abschnitt zu ILIAS vervollständigt.

Das Kapitel <q>Software im Einsatz an Schulen</q> beherbergt jetzt den
Abschnitt über Software an Schulen in Grossbritannien (verschoben von
<q>Bildung</q>) und das FUSS-Projekt wurde hinzugefügt.

Die Projektwebsite wird jetzt mit dem Static Site Generator <q>Pelican</q>
anstatt mit reinem HTML gebaut.

Weitere Änderungen: Bei Referenzen verwenden wir durchgehend Wikipedia als
Autor für Wikipedia-Artikel. Einleitung, Abstract, Bildung wurden reformuliert.
