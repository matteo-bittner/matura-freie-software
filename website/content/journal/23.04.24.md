Title: Ergänzungen bis zur Abgabe
Date: 2023-04-24
ID: 23.04.24

- Appendix (Glossar, Akronyme)
- Antworten Umfrage
- Umfrage als eigenes Kapitel
- Projektwebsite (Abstract)
- GitLab, Moodle, BigBlueButton, LibreOffice inhaltlich ergänzt
- Geschichte von Linus Torvalds
- Freie Software am Georg-Büchner-Gymnasium (Hannover)
- Ausbildung (vs. Bildung)
- Einleitung (Zukunft gestalten, Zitat)
- Geschichte von FSF, OSI, Lizenzen (Kap. Freie Software)
- Massnahmen: Fokuswoche, IT-Support, Schulstick, LaTeX
- Einfluss auf das Bildungswesen (Problemstellung)
- Selbstversuche: Umfragesoftware
- Formatierung: Nice Quotes
- prepare-submission.sh zum Einreichen bei PMS Kreuzlingen
