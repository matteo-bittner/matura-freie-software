\selectlanguage{ngerman}%
\chapter{Wozu Bildung dient}%
\label{chap:wozu-bildung-dient}

In diesem Kapitel betrachten wir das Thema \glqq{}Software\grqq{} im Rahmen
der Allgemeinbildung. Ist es klug, den Vorgaben, Vorschlägen oder Wünschen
der lokalen Wirtschaft zu folgen oder gibt es einen \glqq{}höheren Auftrag\grqq{}?
Was soll die Schule einer Maturandin und einem Maturanden ins Leben mitgeben?

Wir wissen aus der täglichen schulischen Realität, dass ein grosser Teil der
Software, die wir an Schulen einsetzen, von bekannten international tätigen
Firmen stammt (z.B. \organisation{Microsoft}, \organisation{Google},
\organisation{Apple}). Das bestätigen praktisch alle in der Schweiz befragten
Schulen (siehe \autoref{chap:umfrage}). Zum Teil lehren unsere Schulen sogar
bewusst ganz bestimmte Produkte (z.B. \product{Microsoft Word},
\product{Excel}, \product{PowerPoint}) und meiden Alternativen, gestützt auf
das Argument, die Wirtschaft wolle das so, oder weil der Lehrkörper,
Schüler:innen oder Eltern sich wünschen, was sie selbst \glqq{}zu Hause
haben\grqq{}.

Das Einführen und Nutzen von Freier Software ist unpopulär. Das alleine schon
macht die Verwendung schwierig, denn es besteht ein gewisser Gruppenzwang.
Alternativen sind grösstenteils unbekannt, gelten als Terrain für Nerds und
Programmierer. Darüber hinaus ist, wenn man einen Computer im Geschäft kauft,
Software in der Regel schon vorinstalliert. Bei \organisation{Apple}-Hardware
(z.B. \product{MacBooks}) aus technischen Gründen \organisation{Apple}-Software,
bei anderen Marken (z.B. \organisation{Dell}, \organisation{Lenovo},
\organisation{HP}, \organisation{Acer}) ist es \product{Microsoft Windows}, das
gebündelt mit der Hardware verkauft wird. Man kann als Käufer auch nicht selbst
wählen, ob man die Software dazukaufen möchte oder nicht (im Fachjargon spricht
man von der \glqq{}Windows tax\grqq{}, einer steuerähnlichen Abgabe beim Kauf).%
\footcite[Vgl.][]{wikipedia:windows-tax}
Das Deinstallieren der Software und Rückfordern der Kosten ist rechtlich
möglich, in der Umsetzung aber schwierig, wie ausführlich in mehreren Fällen
dokumentiert.%
\footcite[Vgl.][]{fsfe:windows-tax-refund}

Warum man sich grundsätzlich für \glsentry{freie-software} entscheiden soll,
haben wir im \autoref{chap:software} erörtert. Warum soll man die Mühen, die
damit verbunden sind, aber auf sich nehmen? Welchen \glqq{}höheren Auftrag\grqq{},
falls es einen solchen gibt, definiert hierbei die Idee der Allgemeinbildung?

\section{Definition}

Bildung ist ein Begriff, der vielseitig definiert wird.

\begin{nicequote}
Bildung kann im Kern als Maß für die Übereinstimmung des persönlichen Wissens
und Weltbildes eines Menschen mit der Wirklichkeit verstanden werden. Je höher
die Bildung ist, desto größer wird die Fähigkeit, Verständnis für Zusammenhänge
zu entwickeln und wahre Erkenntnisse zu gewinnen.
\footcite[][S. 25]{boehm:theorie-der-bildung}
\end{nicequote}

\subsection{Ausbildung}

Unter \emph{Ausbildung} versteht man den Erwerb von Fachwissen, das Erlernen
des Beherrschens eines gewissen Fachs, einer Maschine, eines Programms, einer
konkreten Tätigkeit. Eine Fähigkeit, Zusammenhänge zu erkennen und Erkenntnisse
zu entwickeln, ist dafür nicht notwendig.

Ausbildung steht im Kontrast zu \emph{Bildung}. Ausbildung ist \emph{Training}.
Bildung kann man nicht trainieren. Bildung braucht Zeit, Zeit zur Reflexion.

\section{Geschichtliche Vorbilder}

\subsection{Immanuel Kant}

Der vielleicht einflussreichste deutsche Philosoph der Aufklärung, Immanuel
Kant (1724-1804), erklärt den Erziehungsbegriff im Sinn der Aufklärung. In
einem 1784 veröffentlichten Artikel in der Berlinischen Monatsschrift spricht
er von der Bequemlichkeit der Unmündigkeit.%
\footcite[][]{kant:was-ist-aufklärung}

\begin{nicequote}
Aufklärung ist der Ausgang des Menschen aus seiner selbst verschuldeten
Unmündigkeit. Unmündigkeit ist das Unvermögen, sich seines Verstandes ohne
Leitung eines anderen zu bedienen. Selbstverschuldet ist diese Unmündigkeit,
wenn die Ursache derselben nicht am Mangel des Verstandes, sondern der
Entschließung und des Mutes liegt, sich seiner ohne Leitung eines anderen zu
bedienen. \glq{}Sapere aude! Habe Mut dich deines eigenen Verstandes zu
bedienen!\grq{} ist also der Wahlspruch der Aufklärung.
\end{nicequote}

Man könnte davon das Selbstverständnis der Lehrpersonen ableiten, das Kant von
aufgeklärten Menschen verlangt. Sie sollen durchdringen, was ihr Leben und das
ihrer Schüler:innen bestimmt, heute insbesondere die Informationstechnologie.
Raus aus der \glqq{}Comfort Zone\grqq{}! Nicht auf das Urteil von Experten
sollen sie sich verlassen, sondern sich selbst ein Urteil bilden und Wege
suchen, mitbestimmen und mitgestalten zu können.

\begin{nicequote}
Faulheit und Feigheit sind die Ursachen, warum ein so großer Teil der Menschen,
nachdem sie die Natur längst von fremder Leitung frei gesprochen (naturaliter
maiorennes), dennoch gerne zeitlebens unmündig bleiben; und warum es Anderen so
leicht wird, sich zu deren Vormündern aufzuwerfen. Es ist so bequem, unmündig
zu sein. Habe ich ein Buch, das für mich Verstand hat, einen Seelsorger, der
für mich Gewissen hat, einen Arzt, der für mich die Diät beurteilt, u.s.w., so
brauche ich mich ja nicht selbst zu bemühen. Ich habe nicht nötig zu denken,
wenn ich nur bezahlen kann; andere werden das verdrießliche Geschäft schon für
mich übernehmen.
\end{nicequote}

Die Aufklärung verlangt die Einbeziehung der eigenen Bemühung in Bildung,
Religion und den Wissenschaften. Der geistige Wandel stellt damit auch die
damals vorherrschenden autoritären Verhältnisse infrage, was wenig später in
den Ausbruch der Französischen Revolution mündet. Es war der Aufbruch in eine
demokratische Welt. Kant möchte, dass man lernt, zu hinterfragen, um
Verantwortung für seine Entscheidungen übernehmen zu können. Anstatt sich
bequem auf fertige Lösungen zu verlassen, soll man selbst entscheiden. Das
bedeutet aber auch, dass man sich die Mühe machen muss, seine Wahlmöglichkeiten
durchzudenken und sich anschliessend der Konsequenzen seiner Wahl im Klaren zu
sein.

\begin{nicequote}
Es ist also für jeden einzelnen Menschen schwer, sich aus der ihm beinahe zur
Natur gewordenen Unmündigkeit herauszuarbeiten. Er hat sie sogar lieb gewonnen
und ist vor der Hand wirklich unfähig, sich seines eigenen Verstandes zu
bedienen, weil man ihn niemals den Versuch davon machen ließ. Satzungen und
Formeln, diese mechanischen Werkzeuge eines vernünftigen Gebrauchs oder
vielmehr Mißbrauchs seiner Naturgaben, sind die Fußschellen einer
immerwährenden Unmündigkeit. Wer sie auch abwürfe, würde dennoch auch über den
schmalsten Graben einen nur unsicheren Sprung tun, weil er zu dergleichen
freier Bewegung nicht gewöhnt ist. Daher gibt es nur Wenige, denen es gelungen
ist, durch eigene Bearbeitung ihres Geistes sich aus der Unmündigkeit heraus
zu wickeln und dennoch einen sicheren Gang zu tun.
\end{nicequote}

Kant sagt, man muss die Menschen daran gewöhnen, die Mündigkeit zu
praktizieren. Auf die uns im heutigen Alltag umgebenden IT-Systeme gemünzt,
kann dies bedeuten, dass wir die \emph{Vier Freiheiten von Software}
praktizieren lernen. Das Nutzenlernen von Programmen alleine ist im Kant'schen
Verständnis zu wenig. Dadurch dass uns die innere Funktionsweise der Software
nicht bekannt ist (\emph{Freedom to study}), können wir die Konsequenzen nicht
abschätzen. Ohne am Prozess der Verbesserung von Software teilzunehmen
(\emph{Freedom to improve}), bemühen wir uns zu wenig und bleiben unmündige
\glqq{}User\grqq{} unserer Software.

All dies klingt überzogen für den Fall, dass uns nur
\glsentry{proprietary-software} zur Verfügung steht. Heute ist die Realität
aber eine andere: Es gibt für wahrscheinlich jede populäre \emph{Closed
Source}-Software eine brauchbare freie, \glsentry{open-source}-Alternative.
Das bedeutet, wir \emph{können} uns bemühen. Frei nach Kant hat die Schule
damit die Aufgabe, die Schüler:innen diese Mündigkeit üben und festigen zu
lassen, also den Umgang mit den \emph{Vier Freiheiten} praktisch zu lehren.

\subsection{Wilhelm von Humboldt}

Ein weiterer einflussreicher Denker der Aufklärung ist der deutsche Gelehrte
Wilhelm von Humboldt (1767-1835). Er hat im damaligen Preussen mit der Berliner
Universität sein Bildungsideal realisiert. Es stellt die geistige Reifung über
das rein zweckorientierte Lernen, also die Bildung über die Ausbildung.%
\footcite[Vgl.][]{planet-wissen:wilhelm-von-humboldt}
Dazu schreibt Humboldt dem preussischen König:%
\footcite[][S. 87]{berglar:wilhelm-von-humboldt}

\begin{nicequote}
Es gibt schlechterdings gewisse Kenntnisse, die allgemein sein müssen, und noch
mehr eine gewisse Bildung der Gesinnungen und des Charakters, die keinem fehlen
darf. Jeder ist offenbar nur dann ein guter Handwerker, Kaufmann, Soldat und
Geschäftsmann, wenn er an sich und ohne Hinsicht auf seinen besonderen Beruf
ein guter, anständiger, seinem Stande nach aufgeklärter Mensch und Bürger ist.
Gibt ihm der Schulunterricht, was hierfür erforderlich ist, so erwirbt er die
besondere Fähigkeit seines Berufs nachher so leicht und behält immer die
Freiheit, wie im Leben so oft geschieht, von einem zum andern überzugehen.
\end{nicequote}

Im Geiste Wilhelm von Humboldts gesprochen, ist das aktive Auseinandersetzen
mit Freier Software keine Aufgabe allein des Informatikunterrichts oder gar
spezialisierter Mittel- und Hochschulen: Es ist eine Aufgabe der Allgemeinbildung.

Die Kenntnis der Möglichkeiten von und der Einflussmöglichkeiten auf (Freie)
Software muss Thema der Allgemeinbildung sein, gerade weil \acrshort{IT} und
\acrshort{IT}-Services einen derart invasiven Charakter haben und unsere
Gesellschaft dramatisch verändern.

\subsection{Richard David Precht}

Der deutsche Philosoph Richard David Precht kritisiert, dass unsere
Gesellschaft die Digitalisierung nahezu ausschliesslich als ein technisches
Problem der wirtschaftlichen Konkurrenzfähigkeit begreift.

Er spricht von einer gesamtgesellschaftlichen Herausforderung, in anderen
Worten müssen die Entscheidungsträger von morgen diese Herausforderungen heute
verstehen lernen, um morgen die richtigen Entscheidungen treffen zu können:
Digitalisierungsunterricht als Allgemeinbildung an unseren Schulen.

\begin{nicequote}
Erbe der Aufklärung {\normalfont [ist]}, sich die Zukunft von Menschen
gestaltet zu denken, sie nicht in Gottes Hand oder in die Hand \glqq{}einer
eigengesetzlichen Evolution von Technologie\grqq{} zu legen.%
\footcite[][]{wikipedia:richard-david-precht}
\end{nicequote}

Konkret heisst das, wir sollen uns nicht den Entwicklungen der IT-Branche
hingeben und als \glqq{}Technologietouristen\grqq{} an einer Reise teilnehmen,
von der wir das Ziel nicht kennen. Es ist notwendig, dass wir Verantwortung
übernehmen und zu verstehen beginnen, dass wir die Zukunft mitgestalten können.
