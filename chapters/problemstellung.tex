\selectlanguage{ngerman}%
\chapter{Problemstellung}%
\label{chap:problemstellung}

Software nimmt in zunehmendem Mass Einfluss auf unser Leben. Gleichzeitig
können wir -- Schüler:innen, Lehrpersonen, Eltern und Entscheidungsträger in
unserer Gesellschaft -- immer weniger einschätzen, was die Software tut und
welche Alternativen wir haben.

\section{Produktnamen statt Technologiebegriffe}

Wir beschreiben Technologien meist mit Produktnamen (z.B. \glqq{}Word\grqq{},
\glqq{}Excel\grqq{}, \glqq{}PowerPoint\grqq{}) und kennen in der Regel nur zwei
Alternativen für Systeme (z.B. \glqq{}Windows oder Apple\grqq{}, \glqq{}iOS
oder Android\grqq{}), beides meist Produkt- oder Markennamen von grossen
IT-Konzernen. Müssten wir nicht eigentlich von \glqq{}Textverarbeitung\grqq{},
\glqq{}Tabellenkalkulation\grqq{} und \glqq{}Präsentationssoftware\grqq{}
sprechen?

Woher kommt diese Produkthörigkeit? Ist es nur Gewohnheit? Oder kennen wir die
Alternativen gar nicht, weil nicht darüber gesprochen wird?

\section{Wer kontrolliert unsere Daten}

Auch haben eine Reihe grosser Unternehmen (\glsentry{big-tech}) in den letzten
Jahren begonnen, Einfluss auf beträchtliche Bereiche unseres Lebens zu nehmen.
Die bekanntesten Namen davon sind: \organisation{Google}
(\organisation{Alphabet}), \organisation{Amazon}, \organisation{Apple},
\organisation{Facebook} (\organisation{Meta}), \organisation{Microsoft}.

Inanspruchnahme von Dienstleistungen und sogar soziale Partizipation wird in
vielen Bereichen bald nur mehr online möglich sein (z.B. e-Government, eHealth,
Online Support bei diversen Unternehmen). Problematisch dabei ist, wie man
immer wieder erfährt, dass für diese Dienstleistungen Technologie und
Infrastruktur von \glsentry{big-tech} zur Verwendung kommt (z.B.
\product{Google Analytics}, Amazon Cloud (\product{AWS}), Microsoft Cloud
(\product{Azure}), Google Cloud (\product{GCP})), weil Unternehmen zur
Optimierung in die Cloud drängen. Man kann sich der Kontrolle von
\glsentry{big-tech} deshalb kaum mehr entziehen.

\section{Adaptive Learning}

Stefano Barale, Informatik-Lehrer an einem Gymnasium mit Schwerpunkt Informatik
in Turin, erklärt in einem Vortrag Ende März 2023 die Gefahren, die vom Konzept
des \glsentry{adaptive-learning} ausgehen. Er nimmt dabei Bezug insbesondere auf
die beiden \glsentry{big-tech}-Grössen \organisation{Microsoft} und
\organisation{Google}, die mit ihren Produkten \product{Microsoft 365 for
Education} und \product{Google Workspace for Education} Vorreiter in dem Gebiet
sind.%
\footcite[Vgl.][]{peertube:fuss}

Die Idee des \glsentry{adaptive-learning}, die den Frontalunterricht um das
individuelle Lernen ergänzt, ist nicht neu und grundsätzlich gut.
\glsentry{big-tech} zeigt, was mit der heutigen Technologie, Big Data und KI,
schon möglich ist. Einen Eindruck darüber liefert die Dokumentation von
\organisation{Microsoft}s \emph{Education Insights}%
\footcite[Vgl.][]{microsoft:education-insights}
und der \emph{Microsoft Education Blog}.%
\footcite[Vgl.][]{microsoft:data-ai-edu}
Zu \product{Google Workspace for Education} finden sich weniger Details zu den
Daten, die gesammelt werden, lediglich Beteuerungen, dass Datenschutzregeln
eingehalten werden und keine Werbung ausgespielt wird.%
\footcite[Vgl.][]{google:workspace-edu}
Allerdings war \organisation{Google} mit seinem Produkt schon vor einem
US-Gerichtshof und musste sich Ende 2021 verpflichten, Anpassungen an
\product{Workspace for Education} umzusetzen und Administrationsmöglichkeiten
für die Sammlung von Daten im Produkt einzubauen.%
\footcite[Vgl.][]{blog:google-collect-data}

Das Problem dabei ist, so Barale, dass die \organisation{Microsoft}- und
\organisation{Google}-Tools Daten im grossen Stil sammeln. Details dazu gibt
\organisation{Microsoft} auf seinen Supportseiten bekannt.%
\footcite[Vgl.][]{microsoft:edu-insights-premium}
Barale meint, \organisation{Microsoft} hätte in diesem Thema im Vergleich zu
\organisation{Google} aktuell die Nase vorn und bezieht sich dabei vermutlich
darauf, dass das Unternehmen es geschafft hat, \product{Microsoft Teams} in der
Zeit der COVID-Pandemie in vielen Institutionen als täglich benutztes Werkzeug
zu etablieren.

Die angebotenen Tools bieten interessante und nützliche Funktionen. Es ist aber
problematisch, dass man bei einem so wichtigen Aspekt wie der Bildung die
Kontrolle grossen, global agierenden Unternehmen überlässt. Die Datenkontrolle
sollte in der Kompetenz der Schulen oder zumindest der Nationalstaaten bleiben.
Das Betreiben einer Software sollte also im öffentlichen Rahmen stattfinden.
\glsentry{freie-software} wäre insofern ein passender Kandidat für eine
diesbezügliche Lösung.

\section{Einfluss auf das Bildungswesen}

Das Thema \glsentry{digitalisierung} ist in den letzten Jahren auch in der
Schule zum Schlagwort geworden. In der Schweiz ist es im \glsentry{lehrplan-21}
mit dem Modul \emph{Medien und Informatik} präsent.%
\footcite[Vgl.][]{lehrplan:tg:module}
Das ist auch den Digitalkonzernen nicht verborgen geblieben und der Einfluss
dieser auf das Bildungswesen macht sich immer stärker bemerkbar.

Die Situation sieht derzeit so aus, dass Kinder und Jugendliche dem unmündigen
Konsum von \glqq{}Fertiggerichten\grqq{} der \glsentry{big-tech}-Konzerne
\organisation{Apple} und \organisation{Microsoft} ausgesetzt sind. Bereits in
der Primarschule wird ihnen ein Tablet der Marke \organisation{Apple}
ausgehändigt und sie lernen im Informatik-Unterricht, das Office-Paket der
Firma \organisation{Microsoft} zu bedienen.

Grosskonzerne statten Schulen mit ihren Produkten aus und lassen sich
Schüler:innen bereits im jüngsten Alter an ihre Produkte heranzuführen.
Produkte, die in der Schule im Einsatz sind, werden konsequent auch zuhause
eingesetzt. Neukunden werden direkt in jungen Jahren erworben und an die
Grosskonzerne gebunden.

Sie lernen somit nicht die Funktionsweise und die Grundsätze der Software zu
studieren bzw. zu ergründen, was hinter der Software steckt. Sie erfahren
nicht, dass Software modelliert werden kann und an eigene Wünsche und
Bedürfnisse angepasst werden kann. Sie üben nicht das analytische Denken und
die Problemlösung, sondern vielmehr lernen sie sich regungslos an die
Gegebenheiten anzupassen und ihre Ansprüche auszuschalten.

\begin{nicequote}
Die Situation {\normalfont [in der Schule]} ist vergleichbar mit der eines
Bauern, der zukünftigen Bauern beibringt, wie man Produkte im Supermarkt
einkauft und weiterverkauft, statt ihnen das Säen und Ernten beizubringen.%
\footcite[Vgl.][]{digicourage:digitalpakt}
\end{nicequote}
