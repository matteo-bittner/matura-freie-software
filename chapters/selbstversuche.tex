\selectlanguage{ngerman}%
\chapter{Selbstversuche}%
\label{chap:selbstversuche}

Zu Hause verwenden wir auf den Computern \glsentry{freie-software}, wo immer
möglich. Ich kann daher schon länger aus einem Erfahrungsschatz schöpfen.
Zusätzlich konnte ich im Rahmen dieser Arbeit einige neue Probleme hands-on
selbst lösen. Dieses Kapitel dokumentiert meine Erfahrungen.

\section{GNU/Linux am eigenen PC}

GNU/Linux ist ein vollwertiges \glsentry{os}, das den beiden populären,
proprietären Alternativen in Wenigem hintendrein steht. In vielen Aspekten
ist es ihnen technisch deutlich überlegen (z.B. Systemsicherheit, Stabilität,
Ressourcennutzung, Benutzerfreundlichkeit des Desktops).

Schon bevor ich an die Pädagogische Maturitätsschule kam, arbeitete ich an
meinem Computer mit \product{Ubuntu Linux}. Ich war in der Klasse immer der
Einzige, der weder Windows noch \product{macOS} hatte. Meinen Mitschülern ist
dies zwar aufgefallen, es war ihnen aber scheinbar gleichgültig, denn ich wurde
deshalb weder gehänselt noch bewundert. Teilweise kam das Vorurteil auf,
GNU/Linux sei nur für Programmierer oder man müsste, um GNU/Linux zu benutzen,
programmieren können.

Beim IT-Support gab es von einem Mitarbeiter abfällige Kommentare. Ich bekam
die Empfehlung, \product{Windows} zu installieren, weil ich Schwierigkeiten
beim Verbinden mit dem Wi-Fi und mit \product{Citrix} hatte. Ein zweiter
Kollege war verständnisvoller und gab mir Tipps. Die Probleme konnte ich am
Ende eigenständig lösen.

\subsection{Fazit}

Mit \product{Ubuntu Linux} kann ich alle Aufträge an der Schule ohne
Einschränkung erfüllen. \product{Windows}-Software, die von der Schule zur
Verfügung gestellt wird, kann ich mit \product{Citrix} problemlos nutzen. Bei
Konfigurationsproblemen ist man im Wesentlichen auf sich alleine gestellt, weil
sowohl den Kolleg:innen als auch dem IT-Personal das Know-how fehlt. Wie auch
für \product{Windows} und \product{macOS} findet man aber im Internet
Lösungsmöglichkeiten.

Das Hauptargument pro \product{Windows}, die Verfügbarkeit vieler Spiele, ist
für mich wenig relevant. Es reicht mir, am Smartphone Spiele zu spielen. In
Bezug auf Textverarbeitung, Tabellenkalkulation und Präsentationen greife ich
meist auf \product{Google Docs} zurück, anstatt \product{Microsoft Office 365}
in unserer Schulinfrastruktur zu nutzen. Leider ist die
\glsentry{benutzeroberfläche} von \product{LibreOffice} im Vergleich zu
\product{Microsoft Office 365} etwas unästhetisch, weswegen es mir keinen
grossen Spass macht, es generell zu nutzen. In letzter Zeit habe ich das aber
doch getan und habe dabei zu meiner eigenen Überraschung keinen Unterschied in
der Leistungsfähigkeit bemerkt. Das hat mich in meiner Vermutung bestärkt, dass
es keine wahrlich sachlichen Argumente gibt, mit denen LibreOffice abgelehnt
werden kann. Es handelt sich eher um eine Art Gruppenzwang, es gilt als
\glqq{}uncool\grqq{}, etwas anderes als das etablierte \product{Office 365} zu
verwenden.

\section{\LaTeX{} zur Textverarbeitung}%
\label{sec:latex}

Zum Verfassen von grösseren Arbeiten an der Schule habe ich in den letzten drei
Jahren statt \product{Microsoft Word} \LaTeX{} in Kombination mit
\glsentry{gitlab} verwendet.

\LaTeX{}\footnote{\url{https://www.latex-project.org}} ist kein
Textverarbeitungsprogramm, sondern eine Art Programmiersprache.
Man schreibt Text in einfachen Textdateien (mit der Endung \texttt{.tex}) mit
einem Texteditor, wie ihn beispielsweise Programmierer verwenden. Teilen des
Textes kann man mit bestimmten Textsequenzen (\glqq{}Befehlen\grqq{}) eine
besondere Bedeutung geben (z.B. Überschrift, Unterüberschrift, Grafik,
Inhaltsverzeichnis, usw.). Mit einem Übersetzungsprogramm (\glqq{}\LaTeX{}
Compiler\grqq{}) kann man aus dem Text und den enthaltenen Befehlen ein
druckfertiges Dokument (z.B. \acrshort{PDF}) generieren.

\subsection{Einfaches Arbeiten mit VSCodium}

Wir müssen ein Dokument abliefern, am besten ohne Rechtschreibfehler, und
wollen schon während des Schreibens sehen, wie das fertige Dokument aussehen
wird, ohne jedes Mal dafür ein \acrshort{PDF}-Dokument generieren zu müssen.
Ausserdem kennen wir die vielen \LaTeX-Befehle nicht, um Text zu formatieren,
Links und Bilder einzufügen, ein Inhaltsverzeichnis, Literaturverzeichnis,
Abbildungsverzeichnis, usw. darzustellen. Gibt es Programme, die uns dabei
unterstützen können?

\product{VSCodium}\footnote{\url{https://vscodium.com}} ist ein freier
Texteditor; es handelt sich um die von der Community frei paketierte Version
von \product{Microsoft Visual Studio Code}. Mit zwei Extensions für \LaTeX{}
(\product{latex-workshop}, \product{vscode-ltex}) wird er zu einer vollwertigen
Entwicklungsumgebung zum Schreiben von \LaTeX-Dokumenten, wenn man sich eine
gängige \LaTeX-\glsentry{distro} auf seinem Rechner installiert hat. Eine
integrierte Vorschau aktualisiert das \acrshort{PDF}-Dokument während man
tippt. Falsch geschriebene Wörter werden blau unterwellt, auf Mausklick bekommt
man passende Korrekturvorschläge. Ein Seitenpanel bietet \LaTeX-Befehle,
Symbole zum Einfügen und eine Dokumentstruktur zum Navigieren an.

\begin{figure}
    \centering
    \includegraphics[width=.69\linewidth]{latex-codium.png}
    \caption{VSCodium mit \product{latex-workshop} Extension}
\end{figure}

Bei einem grossen Projekt ist es wichtig, die Übersicht nicht zu verlieren.
Dazu kann man das Dokument in die einzelnen Kapitel aufteilen, also eine eigene
Datei pro Kapitel verwalten, die im Hauptdokument inkludiert wird.

\subsection{Einhalten formaler Vorgaben leicht gemacht}

Bei einer wissenschaftlichen Arbeit ist es wichtig, formale Aspekte einzuhalten.
Zum Beispiel müssen das Literaturverzeichnis und alle Referenzen im Dokument
ein bestimmtes Format einhalten. \LaTeX{} macht das Anpassen dieser
Ausgabeformate leicht. Entweder gibt es eingebaute Befehle oder \LaTeX{}-Pakete
("packages"), die eine schnelle Anpassung ermöglichen. Damit genügt meist das
Einfügen weniger Zeilen, um das gewünschte Ergebnis zu erreichen.

\paragraph{Beispiel}
Angenommen, es ist gefordert, dass die Abbildungen durchgehend nummeriert
werden (statt im Format \glqq{}Abbildung \emph{Abschnitt}.\emph{Nummer}:
\emph{Beschreibung}\grqq{}). Recherchiert man im Internet, z.B. mit
\glqq{}\emph{latex list of figures continuous without section numbering}\grqq{},
wird man schnell fündig: Ein Blog, ein Beitrag auf StackExchange und die
Overleaf Dokumentation erklären Lösungsmöglichkeiten, die man einfach
übernehmen kann. Fügt man den entsprechenden Code in das Hauptdokument ein,
passt \LaTeX{} sofort alle Referenzen und das Abbildungsverzeichnis der Arbeit
automatisch an.

\subsection{Fazit}

Ursprünglich kam der Vorschlag zum Verwenden von \LaTeX{} von meinen Eltern.
Sie hatten in ihrer Studienzeit schon damit gearbeitet. Der Einstieg war aber
trotzdem schwierig, denn ich musste ein geeignetes Programm zum Arbeiten mit
\LaTeX{} finden. Zum Glück hatte ich das Problem nicht alleine, denn meine
ältere Schwester wollte auch schon mit \LaTeX{} ihre Maturaarbeit schreiben.
Die Suche nach den Arbeitsinstrumenten wurde somit zur Aufgabe für die ganze
Familie.

Mit dem fertigen Setup, \product{VSCodium} mit Extensions für
Rechtschreibprüfung und automatisches \acrshort{PDF}-Generieren, sowie
\glsentry{gitlab} für die begleitende Projektwebsite, kam ich mit der
Online-Dokumentation und Tutorials im Internet gut voran. Ich war aber dankbar,
wenn ich auf meine Familienmitglieder zurückgreifen konnte, wenn es
Layoutprobleme oder Fehlermeldungen gab, mit denen ich nicht sofort etwas
anfangen konnte. Eine \glqq{}Community\grqq{} braucht es also zum Überwinden
von Schwierigkeiten und zum Teilen des Erfolgs!

Heute möchte ich das Arbeiten mit \LaTeX{} nicht mehr missen.
Das schöne, einheitliche Layout, die Leichtigkeit und Übersichtlichkeit beim
Überarbeiten des Dokuments sind Dinge, die \product{Microsoft Word} nicht
leisten kann. Wenn ich uneinheitlich gelayoutete Dokumente meiner Kolleg:innen
sehe und daran denke, wie sie Rückmeldungen der Lehrperson kurz vor dem
Abgabetermin mühsam einarbeiten müssen, denke ich, dass ich mit \LaTeX{} auf
das richtige Pferd gesetzt habe.

\section{Projektwebsite mit GitLab Pages}%
\label{sec:projektwebsite}

\glsentry{gitlab} stellt mit dem Feature \product{GitLab Pages} eine schnelle
Möglichkeit zum Publizieren einer Website zur Verfügung. Man braucht also
keinen Webserver anzumieten oder selbst zu betreiben, \glsentry{gitlab} tut das
schon für uns.

Technisch gesprochen ist diese Funktionalität auf statische Webseiten
beschränkt, d.h. man kann keine Datenbanken und keine am Server laufenden
Technologien (Programmiersprachen) verwenden. Um mit \acrshort{HTML},
\acrshort{CSS} und JavaScript eine vollwertige Website zu erstellen, kann man
sich mit Static Site Generatoren (\acrshort{SSG}) behelfen.%
\footcite[Vgl.][]{jamstack:ssg-list}

\subsection{Mit SSG von HTML zu MarkDown}

Den Anfang der Projektwebsite habe ich rein mit \acrshort{HTML} gemacht. Die
Struktur und Konfiguration für \product{Gitlab Pages} konnte ich von einem
meiner früheren Schulprojekte übernehmen.%
\footcite[Vgl.][]{gitlab:fabio-deutsch-sla}
Zum Aufhübschen habe ich das Stylesheet \LaTeX{}.css von Vincent Dörig
verwendet.%
\footcite[Vgl.][]{dörig:latex.css}
Damit sieht die Website vom Stil her so aus wie das \acrshort{PDF}-Dokument.

Mit dem Arbeitsjournal ist immer mehr \acrshort{HTML} hinzugekommen. Mit der
Zeit machte es Sinn, das \acrshort{HTML} zu generieren, damit ich mich auf den
Inhalt konzentrieren und ich das Copy-und-Paste von \acrshort{HTML}-Code
vermeiden konnte. Ich entschied mich für den \product{Pelican} \acrshort{SSG},
weil er populär ist und er auf Python basiert.

Begonnen habe ich mit der Umstellung auf \product{Pelican} mit dem Quickstart
aus der \product{Pelican}-Dokumentation.%
\footcite[Vgl.][]{pelican:quickstart}
Damit habe ich die Projektstruktur mit den grundlegenden Dateien erstellt, um
die bisherige Website aus MarkDown heraus zu generieren. Ohne besondere
Konfiguration verwendet \product{Pelican} sein Default-Theme. Um die generierte
Website wieder mit dem \LaTeX{}-Design darzustellen, musste ich nach Anleitung
der Dokumentation%
\footcite[Vgl.][]{pelican:themes}
ein einfaches \product{Pelican}-Theme erstellen.
Der Einfachheit halber habe ich Blog-Funktionalität wie Autoren, Kategorien,
Tags und Archiv weggelassen.

\begin{figure}
    \centering
    \includegraphics[width=.8\linewidth]{gitlab-pages-website.png}
    \caption{MarkDown-Eingabedateien und die SSG-Konfiguration}
\end{figure}

\subsection{Fazit}

Das Aufsetzen des \acrshort{SSG}-Setups ist nicht ganz einfach. Man braucht
etwas Erfahrung oder Unterstützung von jemandem, der so etwas schon einmal
gemacht hat. Sobald man diese Hürde einmal genommen hat, ist das Editieren von
Seiten aber sehr einfach. Man kann sich wirklich auf den Inhalt konzentrieren.
Die Syntax von MarkDown kann man leicht lernen bzw. von bestehenden Dokumenten
(z.B. in \glsentry{open-source}-Projekten) abschauen oder im Web recherchieren.

\section{Zur GNOME Dokumentation beitragen}

Um das GNOME Projekt zu erkunden und eine mehr als nur oberflächliche Recherche
zu machen, habe ich mich nach der \glqq{}Getting Involved in GNOME Engagement\grqq{}
Anleitung auf dem Wiki%
\footnote{\url{https://wiki.gnome.org/Engagement/GettingInvolved}}
näher mit dem Matrix-Kanal auseinandergesetzt.

\subsection{Schwierigkeiten mit der Anmeldung}

Allerdings war die Beschreibung um dem \product{Matrix}-Kanal von GNOME
beizutreten zu irreführend und ich kam nicht weiter. Da das Registrieren beim
GitLab Repository bei mir ebenfalls nicht direkt klappte, habe ich mithilfe des
Accounts meines Vaters, der schon registriert war, ein neues \emph{Issue} im
dazugehörigen \glsentry{repo} eröffnet.%
\footnote{\url{https://gitlab.gnome.org/Teams/Engagement/General/-/issues/154}}

Claudio Wunder, der Maintainer des \glqq{}Engagement-Teams\grqq{}, konnte anhand
meiner Rückmeldungen das Wiki anpassen und ich anhand seiner Erklärungen etwas
dazulernen. Somit habe ich es geschafft mich zu registrieren und der
\glqq{}Quick Start Guide\grqq{} wurde auch für zukünftige Newcomers verbessert.

\begin{figure}[H]
    \centering
    \begin{minipage}{0.48\textwidth}
        \centering
        \includegraphics[width=\linewidth]{wiki-gnome-diff.png}
        \caption{Änderungen am Wiki}
    \end{minipage}\hfill
    \begin{minipage}{0.505\textwidth}
        \centering
        \includegraphics[width=\linewidth]{gitlab-gnome-rep.png}
        \caption{GitLab Repository Issue}
    \end{minipage}
\end{figure}

\subsection{Fazit}

Es war nicht ohne Hürden möglich, bei dem Projekt mitzuarbeiten. Man braucht
einen langen Atem und einen starken Willen, sich nicht von kleinen Rückschlägen
entmutigen zu lassen. Anfangs war es frustrierend, dass ich zwar ein
Benutzerkonto bei \product{Matrix} anlegen konnte, aber damit das Anmelden bei
der GNOME \glsentry{instanz} nicht funktionierte.

Auch bei der \glsentry{gitlab} \glsentry{instanz} klappte das Anmelden nach dem
Anlegen des Kontos nicht sofort, und es war nicht klar, warum. Ich musste erst
mit jemandem vom Projekt Kontakt aufnehmen, um die Probleme zu klären. Am Ende
wurde mir klar, dass dahinter Menschen stehen, die ihre Freizeit für mich und
andere Interessierte opfern.

Wichtig war, dass ich meine Erkenntnisse genutzt
habe, um anderen in der Zukunft das Onboarding zu erleichtern. Dadurch habe ich
mich auch zu einem Teil dieser Community gemacht, die in der Freizeit daran
arbeiten, das Projekt immer weiter zu verbessern.

\section{Korrektur im ILIAS Development README}

Um die Mitarbeit beim ILIAS-Projekt auszuprobieren, habe ich einen
\glsentry{pull-request} auf \glsentry{github} eröffnet, mit dem ich die
Korrektur von einem Schreibfehler im README vorschlage.%
\footnote{\url{https://github.com/ILIAS-eLearning/ILIAS/pull/5670}}

Der \glsentry{pull-request}, eine einfache Textänderung, wurde drei Tage später
von Richard Klees, einem Mitarbeiter des ILIAS-Projekts, gemerged.

\begin{figure}[ht]
    \centering
    \includegraphics[width=.7\linewidth]{github-ilias-rep.png}
    \caption{GitHub Pull Request}
\end{figure}

\subsection{Fazit}

Dieser Beitrag zum \glsentry{open-source}-Projekt ILIAS ist ein Beispiel einer
Änderung, die im Grunde jede:r machen kann. Ich bin über den
\glq{}Rechtschreibfehler\grq{} zufällig gestossen, als ich das Projekt
analysiert habe, um darüber zu schreiben. Man braucht für diesen Beitrag zum
Projekt keinerlei Programmierkenntnisse, es reicht den Text mit der
\glsentry{github}-\glsentry{benutzeroberfläche} zu editieren. Dazu benötigt man
nur auf \glsentry{github} ein Benutzerkonto und etwas Experimentierfreude, neue
Dinge auszuprobieren.

\section{Umfragesoftware selbst entwickelt}%
\label{sec:umfragesoftware}

Für meine Maturaarbeit wollte ich eine Umfrage zum Wissensstand zu und der
Verbreitung von Freier Software in Schweizer Schulen durchführen. Dabei stellte
sich heraus, dass praktisch das gesamte gängige Angebot zum Durchführen einer
Umfrage \glsentry{freemium} Services sind, also in der Regel kostenlos nutzbar,
aber unfreie Software.

Für ein modernes Umfrage-Service benötigt man einen Server, der ständig online
ist, und eine Website oder App-Inhalte ausliefert. Das ist zwar keine
Raketenwissenschaft, aber trotzdem nicht für jedermann so einfach umzusetzen.
Deshalb gibt es viele solcher Services online.

\subsection{Anforderungen}

Um selbst ein Umfrageprogramm anzubieten, benötigt man z.B.

\begin{itemize}
\item Einen Computer (\glsentry{host}), auf dem ein Webserver laufen kann.
\item (\glsentry{open-source}-) Software, die die Funktionalität von Umfragen
    anbietet.
\item Das Programm wird eine Datenbank brauchen, um die Umfragen zu speichern.
\end{itemize}

Ich gehe davon aus, dass unsere Schule Serverinfrastruktur hat, um ein kleines
Programm zu hosten. Die Software, wenn es keine fertige Lösung gibt, müsste man
selbst entwickeln, dabei kann man aber, wenn möglich, schon bestehende Projekte
wiederverwenden.

\subsection{Recherche nach einer FLOSS-Lösung}

Wir brauchen ein Programm, das am besten direkt einsetzbar ist, ohne dass wir
selbst entwickeln müssen. Wenn es das nicht gibt, dann ein Programm in einer
Programmiersprache, die wir schon verstehen oder leicht lernen können, und
das fast schon alles kann, was wir brauchen. Damit wir fehlende oder nicht ganz
passende Features leicht ergänzen oder anpassen können.

In der Schule haben wir die Programmiersprache \product{Python} kennengelernt
und \product{Django} ist ein bekanntes \glsentry{framework} zum Entwickeln von
Websites. Eine Suche im Web nach \glqq{}python django survey open source\grqq{}
erweist sich schon als hilfreich (\autoref{fig:django-survey-websearch}).

\begin{figure}
\centering
\includegraphics[width=.5\linewidth]{django-survey-websearch.png}
\caption{Recherche nach einer Umfrage-Applikation}
\label{fig:django-survey-websearch}
\end{figure}

Der erste Link führt uns auf das Projekt \product{django-survey} von Matt Segal
auf \glsentry{github}.%
\footnote{\url{https://github.com/MattSegal/django-survey}}
Das README erklärt das Programm gut, der erste Screenshot ist schon
vielversprechend. Leider hat das \glsentry{repo} keine Lizenz, der Quellcode
ist damit automatisch von Copyright geschützt, also unfrei. Wir können aber
zumindest den Blogbeitrag lesen, den Matt im README verlinkt hat. Er beschreibt
dort ausführlich die Entwicklung einer Survey-Applikation mit Django. Das
hilft auch weiter. (Danke, Matt!)

Der zweite Link zeigt auf \product{django-survey-and-report}, eine
\glqq{}django survey app\grqq{}, wie das README und der Projektname verraten.%
\footnote{\url{https://pypi.org/project/django-survey-and-report}}
Auch hier gibt es eine ausführliche Beschreibung, die Screenshots werden auf
der Website aber leider nicht angezeigt. Über den \glqq{}Homepage\grqq{}-Link
gelangt man aber zum \glsentry{repo}, wo die Bilder im README richtig
dargestellt werden. Auch dieses Projekt sieht vielversprechend aus. Es steht
unter der \emph{GNU Affero General Public License v3 (AGPL-3.0)} Lizenz, d.h.
wir dürfen den Quellcode frei verwenden.

\subsection{Fazit}

Wenn man sich etwas für Software interessiert, findet man im Internet für
viele Problemstellungen Lösungen, die -- Dank freier Lizenz -- frei verwendbar
sind.

Wie schwierig oder einfach die Inbetriebnahme einer solchen Software ist, muss
sich noch zeigen. In unserem Fall ist, was zu tun ist, aber Schritt für Schritt
beschrieben und wirkt einfach umzusetzen. Es sollte daher möglich sein,
zumindest einen einfachen Prototypen rasch zum Laufen zu bringen.

Denkbar wäre, in einem Schüler:innen-Projekt, daraus eine vollwertige Lösung zu
bauen, die man dann als \glsentry{open-source}-Lösung auch anderen Schulen zur
Verfügung stellt.
