\selectlanguage{ngerman}%
\chapter{Massnahmen}%
\label{chap:massnahmen}

Die in diesem Kapitel vorgeschlagenen Massnahmen sollen erreichen, dass
Schüler:innen, der Lehrkörper und die Schuladministration durch praktisches
Anwenden ein gutes Verständnis von Freier Software aufbauen und festigen
können. Dieses praktische Arbeiten kann sowohl intern an
\glsentry{open-source}-Software als auch extern an \acrshort{FLOSS}-Projekten
geschehen.

Im besten Fall wird die Schule somit selbst zu einem System, das kollaborativ,
wie ein \glsentry{open-source}-Projekt, funktioniert. Die \acrshort{IT} wird
Teil des Lehrkörpers, die \acrshort{IT}-Mitarbeiter konzentrieren sich auf die
Weitergabe ihres Wissen -- und lernen Neues vielleicht auch von den
Schüler:innen.

Es ist empfehlenswert, diese Vorschläge \emph{nicht} als zusätzliche Aufgaben
zu sehen und sie auch \emph{nicht} (nur) dem Informatikunterricht zuzuordnen.
Zum Erlernen und Leben der Prozesse von Freier Software sollte Platz im
Lehrplan gemacht werden und das Thema in allen Fächern präsent sein.

\section{Schulhomepage selbst verwalten}

Wie bei den Beispielen \product{GNOME} und \product{ILIAS} in
\autoref{chap:selbstversuche}, soll es möglich sein, dass Schüler:innen
\glsplentry{issue} melden und Anpassungen vorschlagen. Diese Anpassungen
(\glsplentry{merge-request}) können dann von bestimmten Maintainern (z.B.
Rektor:in) angenommen oder zurückgewiesen werden.

\subsection{Problem}

Gemäss Schulverwaltung der PMS wird die Schulwebsite vom Kanton Thurgau mit
dem Tool \product{GOViS} \acrshort{CMS}%
\footcite[Vgl.][]{govis:website}
verwaltet. Die Schule hat dafür einen \product{GOViS}-Account zur Verfügung
gestellt bekommen und kann damit inhaltliche Änderungen an der Kantonswebsite
im Bereich der Schule vornehmen. Weil mit diesen Accounts direkt Änderungen
durchgeführt werden, können nicht beliebige Personen an der Anpassung der
Inhalte mitwirken.

\subsection{Konkrete Vorgehensweise}

Parallel zur bestehenden Schulwebsite wird eine Website auf Quellcode-Basis
aufgebaut und unter \glsentry{versioncontrol} verwaltet (z.B. auf
\glsentry{github} oder \glsentry{gitlab}). Eine einfache Variante wäre es, die
Website direkt in \acrshort{HTML} zu schreiben, die fortgeschrittene Variante,
einen \acrlong{SSG} zu verwenden. Beides ist mit \product{GitHub Pages} oder
\product{GitLab Pages} einfach möglich.

Schüler:innen werden im Informatikunterricht eingewiesen, wie das Melden von
Fehlern und Verbesserungsvorschlägen gemacht wird
(\glsentry{github}/\glsentry{gitlab} \glsplentry{issue}) oder Anpassungen
direkt vorgeschlagen werden (\glsplentry{pull-request}). Administratives
Personal wird eingewiesen, wie man auf \glsplentry{issue} und
\glsplentry{pull-request} reagiert und diese zu einem positiven Abschluss
bringt.

Schritt für Schritt werden die Seiten der bestehenden \acrshort{CMS}-verwalteten
Website migriert. Die aktuelle Website kann am Schluss als Landingpage bestehen
bleiben. Diese Landingpage kann Inhalte darstellen, die sich nicht mehr ändern.
Alle lebenden Inhalte werden am Schluss von den Schüler:innen, Lehrpersonen
und anderen - auch externen - Personen selbständig gewartet und aktualisiert.
Die Kontrolle behält die Schuladministration und engagierte Lehrpersonen.

\subsection{Vorbilder}

\begin{itemize}
\item FUSS Projektwebsite \\
    \url{https://gitlab.fuss.bz.it/fuss/fuss-website}
\item Projektwebsite dieser Maturaarbeit \\
    \url{https://gitlab.com/fabio-totti-05/matura-freie-software/-/tree/main/website}
\end{itemize}

\section{Fokuswoche \glqq{}Freie Software\grqq{}}

Einmal im Jahr könnte in einer Fokuswoche intensiv das Thema
\glsentry{freie-software} behandelt werden. Es würde sich anbieten, das
parallel zur jährlichen \acrshort{FLOSS}-Konferenz \glsentry{FOSDEM} im Februar
stattfinden zu lassen.

\subsection{Konkrete Vorgehensweise}

\begin{itemize}
\item Fokus in allen Unterrichtsfächern auf \glsentry{freie-software} legen.
\item Informationsmaterial in der ganze Schule plazieren; eine kleine Messe mit
    Messeständen veranstalten, wie bei einer Konferenz.
\item Demos und Kurzvorträge von Projekten, neuen Tools und Linux-Distributionen.
\item Hands-on Sessions for Neulinge, Pair-Programming und Hacking-Sessions.
\item Informationsmaterial von der \acrshort{FSFE} bestellen und auflegen.
\item Vortragende der \acrlong{FSFE} zu Themenvorträgen einladen.
\item Lokale Unternehmen, die mit \acrshort{FLOSS} arbeiten, einladen.
\item Für Interessierte die (virtuelle) Teilnahme an der
    \glsentry{FOSDEM}-Konferenz ermöglichen, erlauben oder sogar anregen.
\end{itemize}

\section{Schüler:innen machen IT-Support}

Schüler:innen lernen die Herausforderungen in der \acrshort{IT} hands-on
kennen.

Das \acrshort{IT}-Fachpersonal kann sein Wissen weitergeben und kann --
zumindest für manuelle Arbeiten -- Entlastung durch Unterstützung engagierter
Schüler:innen und/oder Lehrpersonen erfahren.

\subsection{Problem}

Die Schul-\acrshort{IT} wird häufig von Administrationsmitarbeitern gemanaged.
Wissen fliesst kaum oder nicht zwischen den Schüler:innen und dem Personal.
In Organisationen, in denen dem \acrshort{IT}-Personal Linux-Wissen fehlt,
kann das Wissen von Schüler:innen nicht zum \acrshort{IT}-Personal fliessen.

\subsection{Konkrete Vorgehensweise}

\begin{itemize}
\item Aufwändige manuelle Arbeiten oder Automatisierungsvorhaben (z.B.
    Installation von Linux-Clients oder alte Hardware mit Linux revitalisieren)
    im Rahmen von Projekten oder Praktika abarbeiten lassen.
\item \acrshort{IT}-Mitarbeiter kommen in Klassen und erklären aktuelle
    \acrshort{IT}-Vorhaben unter Einsatz von \glsentry{open-source}-Software.
\item Ämtli für \acrshort{IT}-Support (z.B. Klassen wöchentlich im Wechsel).
\item Sich gemeinsam mit \acrshort{IT}-Personal und Schüler:innen Input von
    anderen Schulen holen.
\end{itemize}

\subsection{Vorbilder}

\begin{itemize}
\item Georg-Büchner-Gymnasium (Hannover) \\
    Siehe \autoref{sec:gymnasium-hannover}
\item Penn High School (Pennsylvania) \\
    Siehe \organisation{Red Hat} Kurzfilm%
    \footcite[][]{redhat:penn-manor-opensource}
\end{itemize}

\section{Lernstick als Schuldesktop}

Schüler:innen lernen das \glsentry{GNU/Linux}-\glsentry{os} kennen, ohne
etwas auf ihrem eigenen PC installieren zu müssen. Dazu kann beispielsweise
der \href{https://www.bfh.ch/de/forschung/forschungsbereiche/lernstick/}{%
Berner Lernstick} zum Einsatz kommen.

\subsection{Problem}

\glsentry{GNU/Linux} als \glsentry{os} ist weniger bekannt als \emph{Windows}
und \emph{macOS}. Das macht die Einführung und Nutzung von Linux-basierter
\acrshort{FLOSS}-Software schwierig, denn die Akzeptanz fehlt.

\subsection{Konkrete Vorgehensweise}

\begin{itemize}
\item Crashkurse in einer Fokuswoche \glqq{}\glsentry{freie-software}\grqq{}.
\item Bewusst Projektarbeiten planen, bei denen \glsentry{freie-software} zur
    Erledigung erforderlich ist, wo also der Stick benötigt wird.
\end{itemize}

\section{Einführung von \LaTeX{} für grössere schriftliche Arbeiten}

Als Ziel festlegen, dass alle Schüler:innen befähigt sind, ihre Maturaarbeit
mit \LaTeX{} umsetzen zu können.

\subsection{Problem}

\LaTeX{} ist in akademischen Kreisen zwar bekannt, in den Schulen und im
privaten Umfeld wird nur \product{Microsoft Word} verwendet.

\subsection{Konkrete Vorgehensweise}

\begin{itemize}
\item Unter dem Titel \emph{Arbeiten mit akademischen Ansprüchen} eine
    Projektarbeit bewusst mit \LaTeX{} machen lassen. Dabei bewusst die Hälfte
    des Zeitaufwandes oder mehr für das Lernen von \LaTeX{} einplanen.
\item Schüler:innen erstellen ein Projekt (Beispielprojekt), zusammen im
    Unterricht.
\item Konzepte und Vorteile von bzw. Umgang mit \glsentry{versioncontrol}
    (\emph{Git}) lernen.
\end{itemize}
