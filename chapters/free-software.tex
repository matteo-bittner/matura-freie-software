\selectlanguage{ngerman}%
\chapter{Was ist Freie Software}%
\label{chap:software}

\glsentry{freie-software} und \glsentry{open-source} sind wichtige Begriffe
unserer Zeit. \Glsentry{proprietary-software} ist ihr Gegenspieler. Was als
soziale Bewegung begann, ist mittlerweile zu einem bedeutenden Wirtschaftszweig
geworden. Selbst die klassischen Beispiele für Produzenten proprietärer
Software, beispielsweise \organisation{Microsoft}, investieren in
\glsentry{open-source}-Software und machen massiv Umsatz mit ihr.%
\footcite[Vgl.][]{build5nines:linux-50percent}

\section{Freie Software}

Der Begriff \glsentry{freie-software} steht für Software mit einer Lizenz, die
dem Empfänger bestimmte Freiheiten gewährt. Richard Stallman, der Gründer der
\organisation{Free Software Foundation} hat diese vier Freiheiten verfasst.

\paragraph{Vier wesentliche Freiheiten}%
\footcite[Vgl.][S. 41]{stallman-and-lessing:free-software-free-society}

\begin{mdframed}[style=e-mail]
\begin{enumerate}
\item Die Freiheit, das Programm auszuführen wie man möchte, für jeden Zweck.
\item Die Freiheit, die Funktionsweise des Programms zu untersuchen und eigenen
    Datenverarbeitungbedürfnissen anzupassen.%
    \footnote{\label{note1}Der Zugang zum Quellcode ist dafür Voraussetzung.}
\item Die Freiheit, das Programm weiterzugeben und damit Mitmenschen zu helfen.
\item Die Freiheit, das Programm zu verbessern und diese Verbesserungen der
    Öffentlichkeit freizugeben, damit die gesamte Gesellschaft davon profitiert.%
    \footref{note1}
\end{enumerate}
\end{mdframed}

\glsentry{freie-software} hat nicht primär damit zu tun, dass sie gratis wäre.
Stallman stellt dies klar, indem er explizit sagt, dass man für das Verteilen
von Freier Software gerne eine Gebühr verlangen darf. Das würde sogar helfen,
die Entwicklung dieser Software nachhaltig zu machen.%
\footcite[Vgl.][S. 18]{stallman-and-lessing:free-software-free-society}

\subsection{Die Sternstunde der \acrlong{FSF}}

Alles begann mit einem Softwareentwickler namens Richard Stallman, einem
Papierstau bei einem Drucker und der Unmöglichkeit, Verbesserungen bzw. den
Einbau von weiteren Features am Quellcode anzubringen.

Im 20. Jahrhundert schrieben, gestalteten und benutzen Softwareentwickler ihre
eigenen Betriebssysteme und stellten den Quellcode anderen zur Verfügung:

\begin{nicequote}
Sharing of software {\normalfont [\dots]} is as old as computers, just as
sharing of recipes is as old as cooking.%
\footcite[][S. 15]{stallman-and-lessing:free-software-free-society}
\end{nicequote}

In den 80er-Jahren kamen moderne Computer mit einem eigenen Betriebssystem auf
den Markt -- der Quellcode war aber nicht mehr sichtbar und frei. Es konnten
keine Änderungen und Anpassungen durchgeführt werden und beim Teilen mit
anderen wurde man zum Piraten. So entstand das \emph{Proprietary-Software
Social System}.

Es wurde zur Norm, dass es verboten war, Software zu ändern oder zu teilen.
Eine Gemeinschaft, in der zum Lösen von Softwareproblemen zusammengearbeitet
wurde, wurde zur Piratengemeinschaft. Das Soziale wurde illegal. Die Idee der
proprietären Software war: Bitte uns, die Anpassungen für dich zu machen.%
\footcite[Vgl.][S. 16]{stallman-and-lessing:free-software-free-society}

Richard Stallman und sein Drucker waren in diesem neuen System gefangen.
Unser Softwareentwickler wollte sein Druckerproblem lösen und bat deshalb den
Lieferanten um den Quellcode für das Drucker-Kontrollprogramm. Prompt erhielt
er eine Absage mit dem Verweis auf das -- von ihm unterzeichnete --
\acrlong{NDA} (\acrshort{NDA}).

Dieser Zwischenfall verärgerte Richard Stallman sehr. Er überlegte, wie er die
Welt aus diesem Dilemma holen konnte. Er musste ein \glsentry{os} entwickeln!
Ohne ein solches konnte man einen Computer nicht verwenden. Es musste ein
\glsentry{os} sein, das Benutzer auf ihre Bedürfnisse anpassen können und mit
ihren Nachbarn teilen durften. Damit konnte man wieder eine Gemeinschaft von
Entwicklern bilden und jede:n dazu einladen -- man würde wieder guten Gewissens
Computer nutzen und gemeinsam Probleme lösen dürfen.%
\footcite[Vgl.][S. 17]{stallman-and-lessing:free-software-free-society}

\begin{quotefigure}{Das Gegenteil von \emph{Copyright -- all rights reserved}}%
{fig:quote-copyleft}
Copyleft---all rights reversed.
\end{quotefigure}

1984 begann Stallman an \acrshort{GNU} zu arbeiten. Ein Jahr später erfand er
das \glsentry{copyleft}.%
\footcite[][S. 21]{stallman-and-lessing:free-software-free-society}
1991 steuerte Linus Torvalds das Herzstück für das \glsentry{os} bei, einen
Unix-kompatiblen Kernel, der nach Linus \product{Linux} genannt wurde. Die
Kombination dieser beiden wichtigen Teile ergab ein vollständiges, freies
\glsentry{os}. Das \acrshort{GNU}-System mit dem \product{Linux}-Kernel wird
seither \glsentry{GNU/Linux} genannt.%
\footcite[Vgl.][S. 26]{stallman-and-lessing:free-software-free-society}

\section{Open Source Bewegung}

1998 entstand neben der Bezeichnung \glsentry{freie-software} auch die von
\glsentry{open-source}. Ein Teil der Community wollte das Missverständnis von
\glqq{}free = gratis\grqq{} aus der Welt schaffen. Andererseits gab es Leute
wie Eric S. Raymond, die mehr die praktischen Aspekte der Software-Freiheit
in den Vordergrund stellen wollten als den gesellschaftspolitischen. Sein Buch
\emph{The Cathedral \& The Bazaar} liefert dazu den notwendigen Anstoss.%
\footcite[Vgl.][]{raymond:cathedral-and-bazaar}

Bruce Perens, der gemeinsam mit Raymond 1998 die \acrlong{OSI} (\acrshort{OSI})
gründete, erklärt im Film \emph{Revolution OS} das Hauptanliegen der Anhänger
der Bewegung: Programmierer möchten beim Verbessern von Software
zusammenarbeiten, ohne sich um \emph{Geistiges Eigentum}, Verträge und Anwälte
kümmern zu müssen. Dafür verzichten sie gerne auf einige ihrere Rechte in
diesem Zusammenhang und lassen einfach die ganze Welt ihre Software nutzen.%
\footcite[Vgl.][3'12'']{youtube:revolution-os}

\paragraph{Open Source Definition}
Analog zu den \emph{Vier Freiheiten} der \acrshort{FSF} hat die \acrlong{OSI}
die \emph{Open Source Definition}. Sie ist etwas umfangreicher als erstere.%
\footcite[Vgl.][]{osi:definition}

\begin{mdframed}[style=e-mail]
\begin{enumerate}
\item Freie Weitergabe (auch Verkauf)
\item Quellcode (zusätzlich zum Binärcode)
\item Abgeleitete Werke (müssen erlaubt sein)
\item Integrität des Quellcodes des Autors (die Lizenz kann verlangen, dass
    geänderter Code nicht mit dem Namen des ursprünglichen Autors
    gekennzeichnet wird)
\item Keine Benachteiligung von Personen oder Gruppen
\item Keine Benachteiligung von Arbeitsgebieten (darf überall genutzt werden)
\item Verteilung der Lizenz (Gültigkeit für alle Empfänger)
\item Die Lizenz darf nicht auf ein Produkt beschränkt sein (Unabhängigkeit vom
    ausgelieferten Zustand)
\item Die Lizenz darf andere Software nicht einschränken
\item Die Lizenz muss technologieneutral sein (Allgemeingültigkeit)
\end{enumerate}
\end{mdframed}

Die Bezeichnungen \glsentry{freie-software} und \glsentry{open-source}
beschreiben grundsätzlich die gleiche Software-Kategorie und unterscheiden
sich lediglich in der Philosophie, in den Werten und nicht zuletzt in den
verschiedenen Kriterien, für die Lizenzen akzeptiert werden.
Die \acrshort{GNU}-Community verwendet weiter die Bezeichnung
\glsentry{freie-software}, um die Tatsache zu unterstreichen, dass nicht nur
der technologische Nutzen wichtig ist, sondern vor allem die Freiheit für
die Gesellschaft.%
\footcite[Vgl.][S. 30]{stallman-and-lessing:free-software-free-society}

Eine ausschöpfende Liste von \acrshort{OSI}-kompatiblen Lizenzen findet sich
auf der Homepage der \acrlong{OSI}.%
\footcite[Vgl.][]{osi:licenses}

\section{Lizenzen}

Eine Software-Lizenz ist ein rechtliches Instrument, das die Nutzung und den
Vertrieb von Software, sowie auch deren Modalitäten und Grenzen definiert, die
ein Eigentümer einem Nutzer einräumt. Dabei ist zu beachten, dass der Nutzer
kein Eigentumsrecht an der Software erwirbt, sondern nur das Recht, sie zu
nutzen.

Diese Rechte können beinhalten:%
\footcite[Vgl.][]{berlios:lizenzen}

\begin{itemize}
\item Allgemeines Nutzungsrecht an der Software; Zahl der Nutzer, Art der
    Nutzung usw.
\item Recht auf Weiterverbreitung der Software
\item Recht auf Veränderung der Binärdateien
\item Recht auf Veränderung des Quellcodes, sofern vorhanden
\item Recht auf Weiterverbreitung veränderter Versionen der Binärdateien oder
    des Quellcodes
\item Recht auf Verbindung (\emph{Linking}) der Binärdateien oder des
    Quellcodes mit anderer Software; statisches Linking vs. dynamisches Linking
\end{itemize}

Es gibt eine grosse Anzahl an Arten von Software-Lizenzen. Der Einfachheit
halber kann man Software-Lizenzen in zwei grosse Gruppen unterteilen:
\Glsentry{proprietary-software} und Freie \& \glsentry{open-source} Software.

\subsection{Proprietäre Software-Lizenzen}

\Glsentry{proprietary} bedeutet \glqq{}im Eigentum befindlich\grqq{} bzw., im
juristischen Sinn, \glqq{}urheberrechtlich geschützt\grqq{}.

Für \glsentry{proprietary-software} bleibt der Code in der Regel geheim und
wird nicht mit dem Programm mitgeliefert. Somit kann der Benutzer den Code
nicht inspizieren, also weder kontrollieren, was der Code macht, noch von der
Implementierung lernen, und auch nicht den Code ändern und verbessern.

Zusätzlich ist es allgemein bekannt, dass \glsentry{proprietary-software} die
Kontrolle zu ihren Vorteilen nutzt und diese den Nutzern als \glqq{}Security%
\grqq{} verkauft.%
\footnote{In Entwicklerkreisen sagt man zu dieser Praxis scherzhaft,
\glqq{}\href{https://en.wikipedia.org/wiki/Security_through_obscurity}{%
Security by obscurity}\grqq{}.}
Wir haben keine Kontrolle über die Updates, die automatisch durchgeführt
werden.

\paragraph{Freeware-Lizenzen}
Freeware-Lizenzen zeichnen sich dadurch aus, dass die Software kostenlos
weitergegeben wird, wobei jedoch nur der Zugriff auf den ausführbaren Code,
nicht aber auf den Quellcode gewährt wird. Der Verzicht des Autors beschränkt
sich also auf die Nutzungsvergütung, nicht aber auf das Urheberrecht.

\paragraph{Shareware-Lizenzen}
Software mit Shareware-Lizenz ist nur anfangs und für einen begrenzten Zeitraum
kostenlos, danach ist für die weitere Nutzung eine Gebühr zu entrichten,
andernfalls verweigert die Software ihren Dienst. Das Programm wird dazu mit
einem geeigneten Algorithmus vorbereitet, der ohne gültige(n) Lizenz(schlüssel)
den Betrieb des Programms beendet oder verweigert. Dass dazu der Quellcode
nicht offengelegt wird, erklärt sich von selbst.

\subsection{Freie und Open Source Software (FOSS) Lizenzen}

\acrshort{FOSS}- oder \acrshort{FLOSS}-Lizenzen sind Lizenzen, die die Nutzung,
Weiterverbreitung und Änderung urheberrechtlich geschützter Werke erlauben.%
\footcite[Vgl.][]{wikipedia:freie-lizenz}

\paragraph{Creative Commons-Lizenzen}
Die \acrlong{CC}-Lizenzen wurden 2001 eingeführt, um den Urheber eines Werkes
zu schützen, aber gleichzeitig dem Nutzer Rechte zu gewähren.
\acrshort{CC}-Lizenzen basieren auf vier Klauseln, die je nach Bedarf
kombiniert werden können, woraus sechs verschiedene Arten von Lizenzen
entstehen können (\autoref{fig:cc-lizenzen}).%
\footcite[Vgl.][]{cc:shareyourwork}

\begin{itemize}
\item Namensnennung (Attribution, \emph{BY})
\item keine Überarbeitung erlaubt (Share-alike, \emph{SA})
\item nur nicht-kommerzielle Nutzung (Non-commercial, \emph{NC})
\item Weitergabe unter gleichen Bedingungen (No derivative works, \emph{ND})
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width=.2\linewidth]{cc-lizenzen-spektrum.png}
\caption{Kombinationsmöglichkeiten für Creative Commons-Lizenzen}
\label{fig:cc-lizenzen}
\end{figure}

\paragraph{Open Source-Lizenzen}
\glsentry{open-source}-Lizenzen werden ohne Zahlung durch den Nutzer gewährt,
und der Nutzer erhält auch den Quellcode, sodass das Programm vollständig zur
Verfügung steht; diese Lizenzen sind nicht exklusiv, unterliegen keinen
zeitlichen und territorialen Beschränkungen, und jede von
\glsentry{open-source}-Programmen abgeleitete Software muss unter den gleichen
Bedingungen, also der gleichen Lizenz, zur Verfügung gestellt werden.

\paragraph{Public Domain}
Diese Lizenz macht aus der Software eine \glqq{}vogelfreie\grqq{} Software. Sie
erlaubt den Benutzern, das Programm inklusive ihrer Änderungen mit anderen zu
teilen. Sie erlaubt den Benutzern aber auch das Programm in
\glsentry{proprietary-software} zu verwandeln. Man kann im Programm Änderungen
und Verbesserungen anbringen und es als proprietäres Produkt verwerten. Die
Personen, die dieses modifizierte Programm erhalten, haben nicht die Freiheit,
die der ursprüngliche Autor seinen Nutzern gab.%
\footcite[Vgl.][S. 89]{stallman-and-lessing:free-software-free-society}

\subsubsection{Copyleft}
Eine \glsentry{copyleft}-Lizenz garantiert jedem Benutzer die vier Freiheiten
von Freier Software und stellt sicher, dass auch jede geänderte und erweiterte
Software wiederum die \glsentry{copyleft}-Lizenz trägt.%
\footcite[Vgl.][S. 89]{stallman-and-lessing:free-software-free-society}

\paragraph{GNU General Public Licence (GPL)}
1989 entwickelte das GNU-Projekt für seine Software eine einheitliche Lizenz,
die folgende Kriterien erfüllen sollte:%
\footcite[Vgl.][]{berlios:lizenzen}

\begin{itemize}
\item Freiheit das Programm für jeden Zweck auszuführen
\item Freiheit den Quellcode zu studieren und anzupassen
\item Freiheit, das Programm zu kopieren
\item Freiheit, das veränderte Programm zu kopieren
\end{itemize}

Diese Freiheiten sollten auch langfristig sichergestellt werden. Zu diesem
Zweck enthält die \acrshort{GPL} zwei wesentliche Klauseln:

\begin{itemize}
\item Jedes Derivat muss ebenfalls vollständig unter der \acrshort{GPL}
    lizenziert werden.
\item Bei Weitergabe des Programmes in Binärform muss der Quellcode des
    gesamten Programms mitgeliefert oder auf Anfrage ausgehändigt werden.
\end{itemize}

\paragraph{LGPL (Light GPL)}
Diese Lizenz entstand als \emph{Library \acrlong{GPL}}, sie wurde jedoch in
\emph{Lesser} umbenannt. Gedacht ist diese Lizenz für \glsentry{freie-software},
die in kommerzielle Projekte eingebracht werden soll.%
\footcite[Vgl.][]{berlios:lizenzen}

\paragraph{Mozilla Public Lizenz}
1998 wurde der Quellcode des Web-Browsers Mozilla freigegeben und erhielt eine
neue Lizenz: Die \emph{MPL}. Diese Lizenz ähnelt aber grundsätzlich der
\acrshort{LGPL} hat aber einige neuartige Klauseln.

\subsubsection{Permissive Licenses}
\glsentry{open-source}-Lizenzen, die weniger Auflagen haben als
\glsentry{copyleft}-basierte Lizenzen, nennt man \emph{freizügige Lizenzen}.
Ableitungen von solcher Software muss nicht unter derselben Lizenz
veröffentlicht werden wie Quellcode des ursprünglichen Werkes. Man darf diese
Werke sogar zu proprietärer Software machen.

Beispiele für freizügige Lizenzen sind \emph{Apache 2.0}, \emph{BSD}, \emph{BSD-2}, \emph{BSD-3}, \emph{MIT}

Die \emph{MIT} gilt als die populärste \emph{permissive license}.
Gemäss eigener Recherche verwenden viele Projekte auf GitHub diese Lizenz.
