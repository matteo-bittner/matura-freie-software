#!/usr/bin/bash -e

echo '=== 1. Prepare document for plagiarism check ==='
for FILE in $(find -name '*.tex'); do
    sed -i -E 's/(Fabio Bittner|Kosta Kyriases|Evelyne Fankhauser|freie-software\.bittner\.it|Arno Germann|Tobias Weber|Chris Nielsen|Brigitte Pallmann|Aleksandar Popov|Martin Huber|Ruedi Herzog|Lea Castiglioni|Marcello Indino|Thomas Rätz|Andreas Gräub|Matthias Küng|Giuliani Debora|Martin Klee|Roland Lüthi|Niklaus Streit|Raphael Barengo|Lukas Strub|Jürgen Mischke|Daniel Burri)/(redacted)/' $FILE
    sed -i -E 's/^( +|)\\includegraphics.*/(image-removed)/' $FILE
done
sed -i 's|.adj..includegraphics.height=1cm..#1..|(icon)|' freie-software.tex

echo '=== 2. Build document for plagiarism check ==='
latexmk -pdf

echo '=== 3. Rename document for plagiarism check ==='
mv -v freie-software.pdf 20K-Bittner-Freie-Software-Reduziert.pdf

echo '=== 4. Restore checked-in state of .tex files ==='
git checkout appendix/*.tex chapters/*.tex *.tex *.bib

echo '=== 5. Build regular document ==='
latexmk -pdf

echo '=== 6. Rename regular document ==='
mv -v freie-software.pdf 20K-Bittner-Freie-Software-Original.pdf

echo '=== 7. Clean up debris from LaTeX build ==='
latexmk -c

echo '=== 8. DONE ==='
git status
ls -1F --color *.pdf
